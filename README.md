# Portfolio GLUD
Codificado y diseñado por: J Andres y Brean Muñoz.

Portfolio GLUD es un esquema (template) para realizar portafolios como paginas web, una necesidad que ha veces se presenta cuando quieres fortalecer tu marca personal como desarrollador.

No esta terminado al 100%

Proyecto para el grupo GLUD de la Universidad Distrital FJC.

Licenciado bajo los terminos de: [GPL3](https://www.gnu.org/licenses/gpl-3.0.en.html) en todo el codigo usado.

Llustraciones con licencia CC BY-SA-NC

### Como Usar
Necesitas: Node.js y npm.

(SSH) Clona el repositorio con 
    `git@gitlab.com:luzxyz/portfolio-glud.git`

(HTTP) Clona el repostiorio con
    `https://gitlab.com/luzxyz/portfolio-glud.git`
    
Instala las dependencias con `npm install`
Corre el servidor de prueba con `npm start`
Contruye el aplicativo con `npm build`
### Control de versiones
+ [git GPLv2](https://github.com/git/git)
#### Ilustraciones, Diseño UI
>El trabajo realizado en el campo visual fue realizado con el soporte de software open source y/o libre
+ [Inkscape GPLv2](https://gitlab.com/inkscape/inkscape)
+ [Penpot MPL](https://github.com/penpot/penpot)

### Fuentes de imagenes
> Las imagenes se obtuvieron de un buscador que permitia filtrar por tipo de licencia. De tal manera que no se compromete legalmente el uso de las imagenes que adaptamos y/o modificamos, bajo terminos no comerciales.

+ [Duckduckgo](https://duckduckgo.com/?q=duck&iax=images&ia=images)

Se usaron imagenes con licencias `CC0`, `CC SA`, `CC NC`, `CC BY` o cualquiera de sus combinaciones. 

### Lenguajes de programacion, Frameworks, Modulos
> Se realizo con el framework de React.js, un framework para desarrollar web en front end.
>
> Se utilizo Node.js y npm para gestionar las dependencias del proyecto, de igual manera para hacer posible el servidor local.

+ [HTML5 - licencia depende del navegador (W3C)](https://www.w3.org/Consortium/Legal/2015/copyright-software-and-document)
+ [CSS3  - licencia depende del navegador (W3C)](https://www.w3.org/Consortium/Legal/2015/copyright-software-and-document)
+ [Javascript/Ecmascript  - BSD](https://262.ecma-international.org/12.0/#sec-copyright-and-software-license)
+ [React.js MIT](https://github.com/facebook/react/)
+ [Node.js MIT](https://github.com/nodejs/node)
+ [npm Artistic License 2.0](https://github.com/npm/cli)
+ [Styled components MIT](https://github.com/styled-components/styled-components)

Por favor leer el articulo de Richard Stallman "La trampa de Javascript".
_Richard Stallman's Javascript Trap_
